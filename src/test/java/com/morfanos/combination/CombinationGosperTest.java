package com.morfanos.combination;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class CombinationGosperTest {

    @Test
    public void testNext() {
        List<Long> items = Arrays.asList(1L, 2L, 3L, 4L);
        ICombination<Long> combination = CombinationFactory.factory(items, 2);
        int count = 0;
        while (combination.hasNext()) {
            combination.next();
            count += 1;
        }
        assertEquals(6, count);
    }

    @Test
    public void testInitIndices5() {
        long n = CombinationGosper.initIndices(5);
        assertEquals(0b11111, n);
    }

    @Test
    public void testInitIndices10() {
        long n = CombinationGosper.initIndices(10);
        assertEquals(0b1111111111, n);
    }

    @Test
    public void testGetIndices1() {
        List<Integer> indices = CombinationGosper.getIndices(0b101010);
        assertEquals(3, indices.size());
        assertEquals(1, (int) indices.get(0));
        assertEquals(3, (int) indices.get(1));
        assertEquals(5, (int) indices.get(2));
    }

    @Test
    public void testGetIndices2() {
        List<Integer> indices = CombinationGosper.getIndices(0b0101010);
        assertEquals(3, indices.size());
        assertEquals(1, (int) indices.get(0));
        assertEquals(3, (int) indices.get(1));
        assertEquals(5, (int) indices.get(2));
    }

}

package com.morfanos.combination;

import java.util.List;

public interface ICombination<T> {

    List<T> next();
    boolean hasNext();

}

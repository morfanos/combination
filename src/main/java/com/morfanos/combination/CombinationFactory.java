package com.morfanos.combination;

import java.util.List;

public class CombinationFactory {

    public static <T> ICombination<T> factory(List<T> items, int n) {
        return new CombinationGosper<>(items, n);
    }

}

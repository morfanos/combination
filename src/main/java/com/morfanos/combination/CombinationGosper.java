package com.morfanos.combination;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class CombinationGosper<T> implements ICombination<T> {

    private List<T> items;
    private long indices;
    private boolean isFirstRun = true;

    CombinationGosper(List<T> items, int n) {
        if (items == null
                || items.size() >= Long.SIZE
                || n < 0
                || n >= Long.SIZE
                || n > items.size()) {
            throw new IllegalArgumentException();
        }

        this.indices = initIndices(n);
        this.items = items;
    }

    @Override
    public List<T> next() {
        if (isFirstRun) {
            isFirstRun = false;
        }

        if (this.indices <= 0) {
            return null;
        }

        List<Integer> indices = getIndices(this.indices);
        List<T> subset = new ArrayList<>();
        for (int index : indices) {
            subset.add(items.get(index));
        }

        return subset;
    }

    @Override
    public boolean hasNext() {
        if (isFirstRun) {
            return true;
        }

        Optional<Long> next = next(indices);
        this.indices = next.orElse(0L);
        return next.isPresent();
    }

    // Gosper's Hack
    // https://en.wikipedia.org/wiki/Combinatorial_number_system#Applications
    private Optional<Long> next(long x) { // assume x has form x'01^a10^b in binary
        long u = x & -x; // extract rightmost bit 1; u =  0'00^a10^b
        long v = u + x; // set last non-trailing bit 0, and clear to the right; v=x'10^a00^b
        if (v == 0) // then overflow in v, or x==0
            return Optional.empty(); // signal that next k-combination cannot be represented
        x = v + (((v ^ x) / u) >> 2); // v^x = 0'11^a10^b, (v^x)/u = 0'0^b1^{a+2}, and x ← x'100^b1^a

        if (Long.SIZE - Long.numberOfLeadingZeros(x) > items.size()) {
            return Optional.empty();
        }

        return Optional.of(x);
    }

    static List<Integer> getIndices(long n) {
        List<Integer> indices = new ArrayList<>();
        int index = 0;
        while (n != 0) {
            if ((n & 1) != 0) {
                indices.add(index);
            }
            index++;
            n = n >>> 1;
        }
        return indices;
    }

    static long initIndices(long n) {
        long indices = 1;
        long index = 0b1;
        while (n > 0) {
            indices |= index;
            index = index << 1;
            n -= 1;
        }
        return indices;
    }

}
